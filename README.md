# Slicer MONAI script experimentation

Experimenting with scripts for the LungAIR project.
This is a proof-of-concept for running MONAI inference within Slicer.
Loading images is done using the readers provided by MONAI;
images and segmentations are converted into slicer volumes (with a single slice) and segmentations.

Load this module into Slicer and run through the buttons in the order displayed.
This is meant for lung segmentation in chest xrays. I have only tested it with png images.
