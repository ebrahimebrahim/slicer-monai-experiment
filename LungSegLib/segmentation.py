# seg_net was created in exploration2.ipynb in https://github.com/ebrahimebrahim/lung-seg-exploration
# this wrapper class will handle loading it and running inference

import monai
from typing import Mapping, Hashable, List
import numpy as np
import torch

monai.utils.misc.set_determinism(seed=9274)

class Model:
  def __init__(self):
    self.seg_net = None

    # The transform here is a hard-coded element that depends on the model arhcitecture,
    # but it also depends somewhat on the nature of the input images (e.g. png, dicom)
    # Later on, I may want to output appropriate transforms as part of the model generation script/notebook,
    # so that everything can be loaded here at once.
    image_size = 512
    self.transform = monai.transforms.Compose([
      monai.transforms.LoadImageD(keys = ['img']),
      monai.transforms.TransposeD(keys = ['img'], indices = (1,0)),
      monai.transforms.AddChannelD(keys = ['img']),
      monai.transforms.ResizeD(
        keys = ['img',],
        spatial_size=(image_size,image_size),
        mode = ['bilinear'],
        align_corners = [False]
      ),
      monai.transforms.ToTensorD(keys = ['img']),
    ])

  def load_model(self, filepath):
    self.seg_net = torch.load(filepath)

  def load_parameters(self, filepath):
    if not self.seg_net:
      raise Exception("Cannot load parameters because model architecture is not defined yet.")
    self.seg_net.load_state_dict(torch.load(filepath))

  def load_img(self, filepath):
    img = self.transform({'img':filepath})['img']
    return img

  def run_inference(self, img):
    self.seg_net.eval()
    seg_pred = self.seg_net(img.unsqueeze(0))[0]
    _, max_indices = seg_pred.max(dim=0)
    return max_indices

