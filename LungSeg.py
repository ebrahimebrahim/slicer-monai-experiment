import os, sys
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import numpy as np


try:
  import monai
  from LungSegLib.segmentation import Model
except ModuleNotFoundError:
  pass # There will be a button to check for and install MONAI


#
# Constants
#

script_path = os.path.dirname(os.path.abspath(__file__))
MODEL_DIR = os.path.join(script_path,"segmentation_models")

# For convenience so I can test things quickly
IMAGE_DIR = "/home/ebrahim/data/chest_xrays/MontgomerySet/CXR_png"



# trial and error to get this right :)
IJK_TO_RAS_DIRECTIONS = [[1,0,0], [0,-1,0], [0,0,-1]]


#
# LungSeg
#

class LungSeg(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "LungSeg"
    self.parent.categories = ["LungAIR Experimentation"]
    self.parent.dependencies = []
    self.parent.contributors = ["Ebrahim Ebrahim (Kitware)"]
    self.parent.helpText = """
Experimenting with MONAI inference within Slicer.
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This work is supported by NIH grant [insert grant number for LungAIR].
"""

#
# LungSegWidget
#

class LungSegWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent=None):
    ScriptedLoadableModuleWidget.__init__(self, parent)
    self.model_initialized = False

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Create a collapsible section button
    collapsibleButton = ctk.ctkCollapsibleButton()
    collapsibleButton.text = "MONAI Experimentation"
    self.layout.addWidget(collapsibleButton)

    # Layout within the path collapsible button
    formLayout = qt.QFormLayout(collapsibleButton)

    # prevent code duplication when adding these buttons
    def add_button(text, tooltip, clickedFunction):
      btn = qt.QPushButton(text)
      btn.toolTip = tooltip
      formLayout.addRow(btn)
      btn.connect("clicked()", clickedFunction)
      return btn

    self.monaiInstalledButton = add_button("Check for MONAI install", "Check if MONAI python package is detected", self.checkMonai)
    self.loadImageButton = add_button("Load image", "Load lung image", self.loadImage)
    self.loadModelButton = add_button("Load model", "Load segmentation PyTorch model", self.loadModel)
    self.runInferenceButton = add_button("Run inference", "Run segmentation model", self.runInference)

    if "LungSegLib.segmentation" in sys.modules:
      self.initializeModel()

  def checkMonai(self):
    try:
      import monai, skimage, tqdm
      from LungSegLib.segmentation import Model
      slicer.util.messageBox("MONAI found! Version: " + str(monai.__version__))
      if not self.model_initialized: self.initializeModel()
    except ModuleNotFoundError:
      wantInstall = qt.QMessageBox.question(slicer.util.mainWindow(), "Missing Dependency", "MONAI or a related dependency was not found. Install it?")
      if wantInstall == qt.QMessageBox.Yes:
        slicer.util.pip_install("monai[skimage,tqdm]")
        try:
          import monai, skimage, tqdm
          from LungSegLib.segmentation import Model
          slicer.util.messageBox("Finished installing MONAI.")
          if not self.model_initialized: self.initializeModel()
        except ModuleNotFoundError:
          qt.QMessageBox.critical(slicer.util.mainWindow(), "MONAI Install Error", "Unable to install MONAI. Check the console for details.")

  def initializeModel(self):
    from LungSegLib.segmentation import Model
    self.model = Model()
    self.model_initialized = True
    print("Model initialized")

  def loadImage(self):
    filepath = qt.QFileDialog.getOpenFileName(slicer.util.mainWindow(), "Open Image", IMAGE_DIR) # TODO add appropriate filter
    if not filepath:
      return
    # slicer.util.loadVolume(filepath) # TODO why won't this work
    self.img = self.model.load_img(filepath)
    img_np = self.img[0].numpy() # The [0] contracts the single channel dimension, yielding a 2D scalar array for the image
    self.volumeNode = create_volume_node_from_numpy_array(img_np, "LungAIR CXR")
    slicer.app.layoutManager().setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutOneUpYellowSliceView) # show only yellow slice view to see img maximized
    slicer.util.setSliceViewerLayers(self.volumeNode) # Make volume node visible (this is like toggling the show/hide eyeball icon)
    slicer.util.resetSliceViews() # reset views to show full image

  def loadModelParameters(self): # TODO Get rid of this if it ends up not being useful.
    filepath = qt.QFileDialog.getOpenFileName(slicer.util.mainWindow(), "Open PyTorch Model", MODEL_DIR) # TODO add appropriate filter
    if not filepath:
      return
    self.model.load_parameters(filepath)
    print("Model parameters loaded from: "+filepath)

  def loadModel(self):
    filepath = qt.QFileDialog.getOpenFileName(slicer.util.mainWindow(), "Open PyTorch Model", MODEL_DIR) # TODO add appropriate filter
    if not filepath:
      return
    self.model.load_model(filepath)
    print("Model loaded from: "+filepath)

  def runInference(self):
    self.seg = self.model.run_inference(self.img)
    print("Inference done, shape:",self.seg.shape)
    self.segNode = create_segmentation_node_from_numpy_array(self.seg.numpy(),{1:"left", 2:"right"},"LungAIR SEG",self.volumeNode)

#
# Utility functions (TODO move to lib later)
#

def create_image_data_from_numpy_array(array, oriented : bool, copy = True):
  """Create a vtk image data object from a numpy array.

  Args:
    array: a contiguous 2D numpy array of scalars to turn into a single-sliced 3D vtkImageData
    oriented (bool): whether to return a vtkOrientedImageData instead of a vtkImageData
    copy (bool): whether to copy the underlying data.
      Keep this on unless you are sure the numpy array is going to remain a valid resource.

  Returns: vtkImageData or vtkOrientedImageData
  """

  # See the following for hints on how this works:
  # https://github.com/Kitware/VTK/blob/master/Wrapping/Python/vtkmodules/util/numpy_support.py

  # Get type, e.g. vtk.VTK_FLOAT
  vtk_type = vtk.util.numpy_support.get_vtk_array_type(array.dtype)

  # Ensure array was contiguous
  assert(array.flags.c_contiguous)

  # Create a vtkDataArray of the correct size
  vtk_array = vtk.vtkDataArray.CreateDataArray(vtk_type)
  vtk_array.SetNumberOfComponents(1)
  vtk_array.SetNumberOfTuples(array.size)

  # I'm not certain that th following assert is needed, but if it ever fails then look here for hints:
  # https://github.com/Kitware/VTK/blob/0d344f312f143e7266ae10266f01470fb941ec96/Wrapping/Python/vtkmodules/util/numpy_support.py#L168
  arr_dtype = vtk.util.numpy_support.get_numpy_array_type(vtk_type)
  assert(np.issubdtype(array.dtype, arr_dtype) or array.dtype == np.dtype(arr_dtype))

  # Set underlying data pointer of the vtkDataArray to point to the underlying data of the numpy array
  array_flat = np.ravel(array)
  vtk_array.SetVoidArray(array_flat, len(array_flat), 1)

  # Deep copy the vtkDataArray so that it doesn't rely on the numpy resource to stay alive
  if copy:
    copy = vtk_array.NewInstance()
    copy.DeepCopy(vtk_array)
    vtk_array = copy

  # Create a vtkImageData and set our vtkDataArray to be its point data scalars
  if oriented:
    imageData = slicer.vtkOrientedImageData()
  else:
    imageData = vtk.vtkImageData()
  imageData.SetDimensions([1] + list(array.shape))
  imageData.GetPointData().SetScalars(vtk_array)

  return imageData


def create_volume_node_from_numpy_array(array, node_name : str):
  """Create a volume node and add it to the scene.

  Args:
    array: a contiguous 2D numpy array of scalars to turn into a single-slice volume node
    node_name: string

  Returns: the added vtkMRMLScalarVolumeNode
  """

  imageData = create_image_data_from_numpy_array(array, oriented = False)

  # Create and add a volume node to the scene, setting our vtkImageData to be its underlying image data
  volumeNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLScalarVolumeNode", node_name)
  volumeNode.SetOrigin([0.,0.,0.])
  volumeNode.SetSpacing([1.,1.,1.])
  volumeNode.SetIJKToRASDirections(IJK_TO_RAS_DIRECTIONS)
  volumeNode.SetAndObserveImageData(imageData)
  volumeNode.CreateDefaultDisplayNodes()
  volumeNode.CreateDefaultStorageNode() # TODO including this causes memory leak, reported on exit. why?

  return volumeNode


def create_segmentation_node_from_numpy_array(array, class_names : dict, node_name : str, vol_node):
  """Create a segmentation node and add it to the scene.
  Args:
    array: a contiguous 2D numpy array consisting of discrete class labels
    class_names (dict): a mapping from class labels (values in array) to name strings
      name strings will be assigned to each segment
    node_name (string): name of the segmentation node that will be created
    vol_node: volume node that this segmentation should be associated to

  Returns: the added vtkMRMLSegmentationNode
  """
  segNode = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLSegmentationNode", node_name)
  segNode.SetReferenceImageGeometryParameterFromVolumeNode(vol_node)
  segNode.CreateDefaultDisplayNodes()
  for class_label in class_names.keys():
    binary_labelmap_array = (array==class_label).astype('int8')
    orientedImageData = create_image_data_from_numpy_array(binary_labelmap_array, oriented = True)
    orientedImageData.SetDirections(IJK_TO_RAS_DIRECTIONS)
    segNode.AddSegmentFromBinaryLabelmapRepresentation(orientedImageData, class_names[class_label])
  return segNode
